# Declare properties here
display.visualizations.custom.viz_dayhourheatmap.dayhourheatmap.curveRadius = <integer>
display.visualizations.custom.viz_dayhourheatmap.dayhourheatmap.colorPalette = <string>
display.visualizations.custom.viz_dayhourheatmap.dayhourheatmap.hourRange = <integer>
display.visualizations.custom.viz_dayhourheatmap.dayhourheatmap.showValue = <boolean>
