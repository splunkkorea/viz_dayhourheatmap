define([
        'jquery',
        'underscore',
        'api/SplunkVisualizationBase',
        'api/SplunkVisualizationUtils',
        'd3'
    ],
    function(
        $,
        _,
        SplunkVisualizationBase,
        SplunkVisualizationUtils,
        d3
    ) {
        return SplunkVisualizationBase.extend({
            initialize: function() {
                this.$el = $(this.el);
                this.$el.addClass('dayhourheatmap');
            },

            getInitialDataParams: function() {
                return ({
                    outputMode: SplunkVisualizationBase.RAW_OUTPUT_MODE,
                    count: 10000
                });
            },

            formatData: function(data) {
                var fields = data.fields;
                var rows = data.results;

                // convert string weekday into number
                var weekday = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
                var convertDayToInt = function(wday) {
                    return isNaN(wday) ? weekday.indexOf(wday.toLowerCase()) : wday;
                }

                for (var i = 0, len = rows.length; i < len; i++) {
                    rows[i]["day"] = convertDayToInt(rows[i]["day"]);
                }

                return data;
            },

            // source code from : http://bl.ocks.org/tjdecke/5558084
            updateView: function(data, config) {
                if (!data || data.results.length < 1) return;

                this.$el.empty();

                var margin = { top: 25, right: 20, bottom: 0, left: 40 },
                    width = this.$el.width() - margin.left - margin.right,
                    // height = this.$el.height() - margin.top - margin.bottom,
                    gridSize = Math.floor(width / 24),
                    height = gridSize * 8 + margin.top + margin.bottom;
                legendElementWidth = gridSize * 2,
                    buckets = 9,
                    days = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
                    times = ["1a", "2a", "3a", "4a", "5a", "6a", "7a", "8a", "9a", "10a", "11a", "12a", "1p", "2p", "3p", "4p", "5p", "6p", "7p", "8p", "9p", "10p", "11p", "12p"]

                var showValue = SplunkVisualizationUtils.normalizeBoolean(this._getEscapedProperty('showValue', config));
                var highlightMax = SplunkVisualizationUtils.normalizeBoolean(this._getEscapedProperty('highlightMax', config));
                var hourShift = this._getEscapedProperty('hourRange', config) || '0';
                var cornerRadius = this._getEscapedProperty('cornerRadius', config) || '4';
                var color = this._getEscapedProperty('colorPalette', config) || '#65a637';
                var colorSeries = this._getColorSeries("#ffffe5", color, 9);

                var svg = d3.select(this.el).append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                var dayLabels = svg.selectAll(".dayLabel")
                    .data(days)
                    .enter().append("text")
                    .text(function(d) {
                        return d;
                    })
                    .attr("x", 0)
                    .attr("y", function(d, i) {
                        return i * gridSize;
                    })
                    .style("text-anchor", "end")
                    .attr("transform", "translate(-6," + gridSize / 1.5 + ")")
                    .attr("class", function(d, i) {
                        return ((i >= 1 && i <= 5) ? "dayLabel mono axis axis-workweek" : "dayLabel mono axis");
                    });

                var timeLabels = svg.selectAll(".timeLabel")
                    .data(times)
                    .enter().append("text")
                    .text(function(d) {
                        return d;
                    })
                    .attr("x", function(d, i) {
                        return i * gridSize;
                    })
                    .attr("y", 0)
                    .style("text-anchor", "middle")
                    .attr("transform", "translate(" + gridSize / 2 + ", -6)")
                    .attr("class", function(d, i) {
                        return ((i >= 7 && i <= 16) ? "timeLabel mono axis axis-worktime" : "timeLabel mono axis");
                    });

                var heatmapChart = function(data) {
                    var maxValue = d3.max(data, function(d) {
                        return +d.value;
                    });
                    var colorScale = d3.scale.quantile()
                        .domain([0, buckets - 1, maxValue])
                        .range(colorSeries);

                    var cards = svg.selectAll(".hour")
                        .data(data, function(d) {
                            return d.day + ':' + d.hour;
                        });

                    cards.append("title");

                    cards.enter().append("rect")
                        .attr("x", function(d) {
                            return (d.hour - hourShift) * gridSize;
                        })
                        .attr("y", function(d) {
                            // return (d.day - 1) * gridSize; // start of a week is 0 or sunday
                            return (d.day) * gridSize;
                        })
                        .attr("rx", cornerRadius)
                        .attr("ry", cornerRadius)
                        .attr("class", "hour bordered")
                        .attr("width", gridSize)
                        .attr("height", gridSize)
                        .style("fill", colorSeries[0]);

                    cards.transition().duration(500)
                        .style("fill", function(d) {
                            if (highlightMax) return (d.value == maxValue) ? "rgb(214, 86, 60)" : colorScale(d.value);
                            return colorScale(d.value);
                        });

                    cards.select("title").text(function(d) {
                        return d.value;
                    });

                    cards.exit().remove();

                    // render values of each card
                    if (showValue) {
                        var valueLabel = svg.selectAll(".valueLabel")
                            .data(data).enter().append("text")
                            .attr("x", function(d) {
                                return (d.hour - hourShift) * gridSize + gridSize / 2;
                            })
                            .attr("y", function(d) {
                                return (d.day) * gridSize + gridSize / 2;
                            })
                            .attr("dy", ".35em")
                            .attr("text-anchor", "middle")
                            .text(function(d) {
                                return d.value;
                            });
                    }

                    // render legend
                    var legend = svg.selectAll(".legend")
                        .data([0].concat(colorScale.quantiles()), function(d) {
                            return d;
                        });

                    legend.enter().append("g")
                        .attr("class", "legend");

                    legend.append("rect")
                        .attr("x", function(d, i) {
                            return legendElementWidth * i;
                        })
                        // .attr("y", height)
                        .attr("y", gridSize * 7.2)
                        .attr("width", legendElementWidth)
                        .attr("height", gridSize / 2)
                        .style("fill", function(d, i) {
                            // return colors[i];
                            return colorSeries[i];
                        });

                    legend.append("text")
                        .attr("class", "mono")
                        .text(function(d) {
                            return "≥ " + Math.round(d);
                        })
                        .style("alignment-baseline", "text-before-edge")
                        .attr("x", function(d, i) {
                            return legendElementWidth * i;
                        })
                        .attr("y", gridSize * 7.7);

                    legend.exit().remove();
                };
                heatmapChart(data.results);

                // return this;
            },

            reflow: function() {
                this.invalidateUpdateView();
            },

            _getEscapedProperty: function(name, config) {
                var propertyValue = config[this.getPropertyNamespaceInfo().propertyNamespace + name];
                return SplunkVisualizationUtils.escapeHtml(propertyValue);
            },

            _getColorSeries: function(c1, c2, bucket) {
                var colors = [];
                bucket = bucket < 1 ? 1 : bucket;

                // http://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors
                var shadeBlend = function(p, c0, c1) {
                    var n = p < 0 ? p * -1 : p,
                        u = Math.round,
                        w = parseInt;
                    var f = w(c0.slice(1), 16),
                        t = w((c1 ? c1 : p < 0 ? "#000000" : "#FFFFFF").slice(1), 16),
                        R1 = f >> 16,
                        G1 = f >> 8 & 0x00FF,
                        B1 = f & 0x0000FF;
                    return "#" + (0x1000000 + (u(((t >> 16) - R1) * n) + R1) * 0x10000 + (u(((t >> 8 & 0x00FF) - G1) * n) + G1) * 0x100 + (u(((t & 0x0000FF) - B1) * n) + B1)).toString(16).slice(1)
                }

                for (var i = 0; i < bucket; i++) {
                    colors.push(shadeBlend(1 / bucket * i, c1, c2));
                }

                return colors;
            }
        });
    });
