# README #

Day/Hour Heatmap Custom visualization app for Splunk.

### Overview ###

A Day/Hour Heatmap shows metric behavior over a week time by days and hours. You can find at which hour of the whole week certain events most/least occurred. This visualization was intended to display total incoming calls to the call center of online shopping malls to efficiently adjust resources.

This Splunk app is a simple implementation of a visualization from d3 example. http://bl.ocks.org/tjdecke/5558084

### Features ###

* Change the color series of the card
* Change the corner radius of the card
* Toggle to show the value label
* Select the hour scheme
* Highlight max value

### How to install ###

* cd $SPLUNK_HOME/etc/apps
* git clone -b master https://<your_bitbucket_id>@bitbucket.org/splunkkorea/viz_dayhourheatmap.git

### How to use ###

* Splunk Splunk search that exposes three fields named as "day", "hour" and "value"
* Example. index=_internal earliest=-1w@w latest=@w | stats count by date_wday, date_hour | rename date_wday as day, date_hour as hour, count as value

### Reference ###

* http://bl.ocks.org/tjdecke/5558084
* http://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors

### Who do I talk to? ###

* For bugs or enhencements, please create an issue below
* https://bitbucket.org/splunkkorea/viz_dayhourheatmap/issues?status=new&status=open